<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Auth;
use Session;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // get all the tasks
        $tasks = Task::where('assignee_id', Auth::user()->id)->get();

        // load the view and pass the tasks
        return view('tasks.index', ['name' => Auth::user()->name, 'tasks' => $tasks]);
//        return $tasks;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        /* $validator */
        // store
        $task = new Task();
        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->due_date = $request->input('due_date');
        $task->priority = $request->input('priority');
        $task->status = 'toDo';
        $task->assignee_id = $request->user()->id;
        $task->save();

//        return redirect()->route('tasks.index');
        // redirect
        Session::flash('message', 'Successfully created task!');
        return redirect('/tasks');
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        return view('tasks.show', ['name' => Auth::user()->name, 'task' => $task]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Task $task)
    {
        return view('tasks.edit', ['task' => $task]);
//        return $task;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Task $task)
    {

        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->due_date = $request->input('due_date');
        $task->priority = $request->input('priority');
        $task->status = $request->input('status');
        $task->save();

//        return redirect()->route('tasks.show', $task);
        Session::flash('message', 'Updated task details!');
        return redirect('/tasks');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        $task->delete();
        Session::flash('message', 'Task deleted!');
        return redirect('/tasks');
    }

    public function patchTask(Request $request, Task $task)
    {
         if(null !==$request->input('status')) $task->status = $request->input('status');
         elseif(null !==$request->input('priority')) $task->priority = $request->input('priority');
         $task->save();
        Session::flash('message', 'Updated task details!');
        return redirect('/tasks');
    }
}
