<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Task;
use Auth;
use Session;

class DashboardController extends Controller
{
    public function index()
    {
        $tasks = Task::where('assignee_id', Auth::user()->id)
            ->where('status', 'inProgress')
            ->where('due_date', '>=', Carbon::now())
//            ->whereRaw('(now() between start_date and end_date)')
            ->paginate(4);
        return view('user.home', compact('tasks'),['tab' => "active"]);
    }

    public function completeTasks()
    {
        $tasks = Task::where('assignee_id', Auth::user()->id)
            ->where('status','completed')
            ->paginate(4);
        return view('user.home', compact('tasks'),[ 'tab' => "complete"]);
    }
    public function backlogTasks()
    {
        $tasks = Task::where('assignee_id', Auth::user()->id)
            ->where('status','inProgress')
            ->where('due_date', '<=', Carbon::now())
            ->paginate(4);
        return view('user.home', compact('tasks'),['tab' => "backlog"]);
    }
    public function archivedTasks()
    {
        $tasks = Task::where('assignee_id', Auth::user()->id)
            ->where('status', 'closed')
            ->paginate(4);
        return view('user.home', compact('tasks'),['tab' => "archived"]);
    }
    public function upcomingTasks()
    {
        $tasks = Task::where('assignee_id', Auth::user()->id)
            ->where('status', 'toDo')
            ->paginate(4);
        return view('user.home', compact('tasks'),['tab' => "upcoming"]);
    }
}
