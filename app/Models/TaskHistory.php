<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskHistory extends Model
{
    use HasFactory;
    protected $fillable = [
        'task_id',
        'field_name',
        'old_value',
        'new_value',
    ];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getOldValueAttribute($value)
    {
        return $this->castValue($value);
    }

    public function getNewValueAttribute($value)
    {
        return $this->castValue($value);
    }

//    private function castValue($value)
//    {
//        if (is_numeric($value)) {
//            return (int) $value;
//        }
//
//        if ($value === 'true') {
//            return true;
//        }
//
//        if ($value === 'false') {
//            return false;
//        }
//
//        return $value;
//    }
}
