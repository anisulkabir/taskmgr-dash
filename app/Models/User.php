<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'photo',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class, 'assignee_id');
    }

    public function sharedTasks()
    {
        return $this->belongsToMany(Task::class, 'shared_tasks');
    }

    public function taskHistories()
    {
        return $this->hasManyThrough(TaskHistory::class, Task::class, 'assignee_id', 'task_id');
    }

    public function getTaskHistoryAttribute()
    {
        return $this->taskHistories()->latest()->first();
    }

    public function getTaskHistoryOldValueAttribute()
    {
        return $this->taskHistory->old_value ?? null;
    }

    public function getTaskHistoryNewValueAttribute()
    {
        return $this->taskHistory->new_value ?? null;
    }

}
