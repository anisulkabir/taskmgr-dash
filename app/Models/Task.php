<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'due_date',
        'priority',
        'status',
        'assignee_id',
    ];

    public function assignee()
    {
        return $this->belongsTo(User::class, 'assignee_id');
    }

    public function sharedUsers()
    {
        return $this->belongsToMany(User::class, 'shared_tasks');
    }

    public function history()
    {
        return $this->hasMany(TaskHistory::class);
    }
}
