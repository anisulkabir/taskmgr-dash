# Task Manager Test-bed
## Prepare for testing
### Install dependencies
- copy the .env.example file to .env and edit the database connection settings
- start your database server
- change directory to the root of the project and sequentially run:
```bash
composer install
npm install
vite build

artisan migrate:fresh --seed
artisan serve

```
#### commands may be different depending on your environment
eg. on Windows you may need to run:
```cmd
composer install
npm install
npm run build

php artisan migrate:fresh --seed
php artisan serve
```

For profile pictures to work, you need to manually copy the contents of ```profile_photos.zip``` to
```/public/storage/profile_photos/```
