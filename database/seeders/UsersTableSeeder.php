<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Kabir Chowdhury',
                'email' => 'kabir@post.com',
                'email_verified_at' => NULL,
                'photo' => '/storage/profile_photos/pic_1697403857.jpg',
                'password' => '$2y$10$SGwBJ9FxglM0ltFSrXXY4eHF4c69rUU/vIuO8lb7fvZ4dlG6xI4sa',
                'remember_token' => 'iwMFkGIf9xkjCTtQPJ7dIuclFQNrlqAIswiFRcpW3v5VIl4BUkZUkpqUtXSj',
                'created_at' => '2023-10-15 02:38:25',
                'updated_at' => '2023-10-15 02:38:25',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Donald Duck',
                'email' => 'donald@mail.com',
                'email_verified_at' => NULL,
                'photo' => '/storage/profile_photos/pic_1697453531.png',
                'password' => '$2y$10$SGwBJ9FxglM0ltFSrXXY4eHF4c69rUU/vIuO8lb7fvZ4dlG6xI4sa',
                'remember_token' => NULL,
                'created_at' => '2023-10-15 04:00:22',
                'updated_at' => '2023-10-15 04:00:22',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Mickey Mouse',
                'email' => 'mm@email.com',
                'email_verified_at' => NULL,
                'photo' => NULL,
                'password' => '$2y$10$SGwBJ9FxglM0ltFSrXXY4eHF4c69rUU/vIuO8lb7fvZ4dlG6xI4sa',
                'remember_token' => NULL,
                'created_at' => '2023-10-16 13:32:11',
                'updated_at' => '2023-10-16 13:32:11',
            ),
        ));
        
        
    }
}