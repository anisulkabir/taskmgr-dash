<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tasks')->delete();
        
        \DB::table('tasks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Task 1',
                'description' => 'Description 1',
                'due_date' => '2023-11-11 23:56:00',
                'priority' => 'medium',
                'assignee_id' => 1,
                'created_at' => '2023-10-15 03:23:40',
                'updated_at' => '2023-10-15 10:30:38',
                'status' => 'toDo',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Task 2',
                'description' => 'Description 2',
                'due_date' => '2023-11-23 00:00:00',
                'priority' => 'medium',
                'assignee_id' => 1,
                'created_at' => '2023-10-15 03:31:29',
                'updated_at' => '2023-10-15 03:31:29',
                'status' => 'closed',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Communicate often',
                'description' => 'Maintain regular communication while working on the project. This can include team meetings, keeping the task list updated and accurate and hosting a virtual group forum to address any concerns that may arise. Communicating often helps your team stay informed and productive and makes your task list more effective.',
                'due_date' => '2023-11-23 00:00:00',
                'priority' => 'medium',
                'assignee_id' => 2,
                'created_at' => '2023-10-15 05:53:16',
                'updated_at' => '2023-10-16 13:10:30',
                'status' => 'toDo',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Reflect on work practices',
                'description' => 'Reflect on a past project\'s task list to help you understand which areas you and your team excel in. Understanding how a team accomplished their goals during each step of the task list can lead to the creation of better working practices, insight on the team\'s dynamic and a plan for future projects.',
                'due_date' => '2023-10-23 00:00:00',
                'priority' => 'low',
                'assignee_id' => 2,
                'created_at' => '2023-10-15 05:53:32',
                'updated_at' => '2023-10-16 13:11:31',
                'status' => 'toDo',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Separate the project into sections',
                'description' => 'Consider how to separate your project into individual tasks. To do this, try to decide if any aspect of the project may need addressing sooner than others. For example, if you\'re working on a project that requires research, writing and editing, it\'s important to first complete the research for the project since professionals cannot write without it.',
                'due_date' => '2023-10-25 00:00:00',
                'priority' => 'medium',
                'assignee_id' => 2,
                'created_at' => '2023-10-15 05:53:51',
                'updated_at' => '2023-10-16 13:12:34',
                'status' => 'inProgress',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Improves efficiency',
                'description' => 'Using a task list ensures teams make the most of their time and resources. Because managers can delegate tasks and professionals can often work on different sections of the same project at the same time, teams often complete projects more efficiently.',
                'due_date' => '2023-11-23 00:00:00',
                'priority' => 'high',
                'assignee_id' => 2,
                'created_at' => '2023-10-15 06:12:11',
                'updated_at' => '2023-10-16 13:13:56',
                'status' => 'inProgress',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Prioritizes tasks',
                'description' => 'Sharing a task list helps teams of professionals consider the priority of individual tasks in relation to the other tasks on the list. This ensures professionals complete individual tasks within their deadlines while allowing the project to progress smoothly.',
                'due_date' => '2023-11-23 00:00:00',
                'priority' => 'high',
                'assignee_id' => 2,
                'created_at' => '2023-10-15 06:12:52',
                'updated_at' => '2023-10-16 13:14:38',
                'status' => 'completed',
            ),
        ));
        
        
    }
}