<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes(['verify' => true]);

Route::middleware([ 'auth', 'verified'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/upcoming', [DashboardController::class, 'upcomingTasks'])->name('upcoming');
    Route::get('/complete', [DashboardController::class, 'completeTasks'])->name('complete');
    Route::get('/backlog', [DashboardController::class, 'backlogTasks'])->name('backlog');
    Route::get('/archived', [DashboardController::class, 'archivedTasks'])->name('archived');
    Route::resource('/tasks', TaskController::class);
    Route::patch('tasks/{task}/patch', [TaskController::class, 'patchTask'])->name('tasks.patch-status');
});

Route::get('/', [HomeController::class, 'index'])->name('home');
