@extends('layouts.app')

@section('helmet')
    All tasks
@endsection
{{--
<?php
echo '<pre>';
print_r($tasks);
echo '</pre>';
?>
--}}

@section('content')
    <section>
        <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" id="toastAlert" data-bs-autohide="false">
            <div class="toast-header">
                <img src="..." class="rounded me-2" alt="...">
                <strong class="me-auto">Bootstrap</strong>
                <small>11 mins ago</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                {{Session::get('message')}}
            </div>
        </div>
        <script>
            var toastLiveExample = document.getElementById('toastAlert')
            if (toastTrigger) {
                toastTrigger.addEventListener('click', function () {
                    var toast = new bootstrap.Toast(document.getElementById('toastAlert'))

                    toast.show()
                })
            }
        </script>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mx-auto">
                    <p class="text-danger text-center my-1 fw-bold">{{Session::get('message')}} <span>&nbsp;</span></p>
                    <div class="card rounded-0">
                        <div class="card-header text-center fw-bolder h5">{{$name}}'s tasks</div>
                        <div class="card-body">
                            <table class="table table-border">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Task Title</th>
                                    <th>Task Description</th>
                                    <th>Due Date</th>
                                    <th>Priority</th>
                                    <th>status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tasks as $task)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td style="max-width: 150px; white-space: nowrap; text-overflow: ellipsis ;overflow: hidden">
                                            <span class="fw-bolder @switch($task->priority)
                                            @case('low') {{ __('text-success') }} @break
                                            @case('medium') {{ __('text-primary') }} @break
                                            @case('high') {{ __('text-danger') }} @break
                                            @default {{ __('') }} @endswitch ">{{$task->title}}</span>
                                        </td>
                                        <td style="max-width: 250px; white-space: nowrap; text-overflow: ellipsis ;overflow: hidden">{{$task->description}}</td>
                                        {{--                                        <td>{{ date_format(strtotime($task->due_date),"Y F d h:i a") }}</td>--}}
                                        <td style="max-width: 120px; white-space: nowrap; text-overflow: ellipsis ;overflow: hidden">{{ Carbon\Carbon::createFromTimestamp(strtotime($task->due_date))->format('d M Y, h:i a') }}</td>
                                        <td style="max-width: 90px">
                                            <div class="dropdown">
                                                <a role="button" class="btn btn-sm btn-outline-primary dropdown-toggle fw-bold text-capitalize w-100
                                                    @switch($task->priority)
                                                    @case('low') {{ __('btn-outline-success') }}@break
                                                    @case('medium') {{ __('btn-outline-primary') }} @break
                                                    @case('high') {{ __('btn-outline-danger') }} @break
                                                    @default {{ __('Logout') }} @endswitch"
                                                   data-bs-toggle="dropdown" aria-expanded="false" id="priorityBtn_{{$task->id}}">
                                                    {{$task->priority}}
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><form action="{{ route('tasks.patch-status', $task->id) }}" method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <input type="hidden" name="priority" value="low">
                                                            <button class="dropdown-item" role="button" type="submit" onclick="return spinnerBtn('priorityBtn_{{$task->id}}')" >Low</button>
                                                        </form>
                                                    </li>
                                                    <li><form action="{{ route('tasks.patch-status', $task->id) }}" method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <input type="hidden" name="priority" value="medium">
                                                            <button class="dropdown-item" role="button" type="submit" onclick="return spinnerBtn('priorityBtn_{{$task->id}}')" >Medium</button>
                                                        </form>
                                                    </li>
                                                    <li><form action="{{ route('tasks.patch-status', $task->id) }}" method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <input type="hidden" name="priority" value="high">
                                                            <button class="dropdown-item" role="button" type="submit" onclick="return spinnerBtn('priorityBtn_{{$task->id}}')" >High</button>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td style="max-width: 90px">
                                            {{--                                        @switch($task->status)
                                                                                        @case('toDo') <span class="badge text-bg-info">To Do</span> @break
                                                                                        @case('inProgress') <span class="badge text-bg-primary">In progress</span>  @break
                                                                                        @case('completed') <span class="badge text-bg-success">Completed</span>  @break
                                                                                        @default <span class="badge text-bg-danger">Closed</span>  @endswitch--}}
                                            <div class="dropdown" >
                                                @switch($task->status)
                                                    @case('toDo') <a role="button" class="btn btn-sm btn-primary dropdown-toggle fw-bold w-100" id="statusBtn_{{$task->id}}" data-bs-toggle="dropdown" aria-expanded="false" > To Do </a> @break
                                                    @case('inProgress') <a role="button" class="btn btn-sm btn-success dropdown-toggle fw-bold w-100" id="statusBtn_{{$task->id}}" data-bs-toggle="dropdown" aria-expanded="false"> In progress </a> @break
                                                    @case('completed') <a role="button" class="btn btn-sm btn-secondary dropdown-toggle fw-bold w-100" id="statusBtn_{{$task->id}}" data-bs-toggle="dropdown" aria-expanded="false">Completed</a> @break
                                                    @default <span class="badge text-bg-danger">Closed</span>  @endswitch
                                                <ul class="dropdown-menu">
                                                    <li><form action="{{ route('tasks.patch-status', $task->id) }}" method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <input type="hidden" name="status" value="toDo">
                                                            <button class="dropdown-item" role="button" type="submit" onclick="return spinnerBtn('statusBtn_{{$task->id}}')" href="">To Do</button>
                                                        </form>
                                                    </li>
                                                    <li><form action="{{ route('tasks.patch-status', $task->id) }}" method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <input type="hidden" name="status" value="inProgress">
                                                            <button class="dropdown-item" role="button" type="submit" onclick="return spinnerBtn('statusBtn_{{$task->id}}')" href="">In progress</button>
                                                        </form>
                                                    </li>
                                                    <li><form action="{{ route('tasks.patch-status', $task->id) }}" method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <input type="hidden" name="status" value="completed">
                                                            <button class="dropdown-item" role="button" type="submit" onclick="return spinnerBtn('statusBtn_{{$task->id}}')" href="">Completed</button>
                                                        </form>
                                                    </li>
                                                    {{--                                                    <li><a class="dropdown-item" href="{{ route('tasks.patch-status', [$task->id, 'inProgress']) }}">In progress</a></li>--}}
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex gap-1">
                                                <a href="{{route('tasks.show',['task' => $task->id])}}"
                                                   data-toggle="tooltip" title="View details"
                                                   class="nav-link px-2 pb-1 border text-center rounded-0">
                                                    <i class="fa-solid fa-users-viewfinder"></i>
                                                </a>
                                                <a href="{{route('tasks.edit',['task' => $task->id])}}"
                                                   data-toggle="tooltip" title="Modify this task"
                                                   class="nav-link px-2 pb-1 border text-center rounded-0">
                                                    <i class="fa-solid fa-edit"></i>
                                                </a>
                                                <form action="{{route('tasks.destroy',['task'=>$task->id])}}"
                                                      method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit"
                                                            href=""
                                                            class="nav-link hover-zoom px-2 pb-1 border text-center text-danger rounded-0"
                                                            data-toggle="tooltip" title="Delete this task"
                                                            onclick="return confirm('Are you sure you want to delete this?')">
                                                        <i class="fa-solid fa-trash-can"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-5">
                <a class="btn btn-primary mx-auto" href="{{route('tasks.create')}}">Add task</a>
            </div>
        </div>
    </section>
    <script>
        function spinnerBtn(id){
            var element = document.getElementById(id);
            // element.setAttribute(disabled);
            element.innerHTML = `
              <span class="spinner-border spinner-border-sm" aria-hidden="true"></span>
              <span role="status">Loading...</span>
            `
        }
    </script>
@endsection
