@extends('layouts.app')

@section('helmet')
    Add task
@endsection


@section('content')
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header text-center">Update Task Form</div>
                        <div class="card-body">
                            <!-- will be used to show any messages -->
                            @if (Session::has('message'))
                                <div class="alert alert-info">{{ Session::get('message') }}</div>
                            @endif
                            {{--                            <p class="text-success text-center">{{Session::get('message')}}</p>--}}
                            <form action="{{ route('tasks.update',['task'=>$task->id]) }}" method="POST" >
                                @csrf
                                @method('PUT')
                                <div class="row mb-3">
                                    <label for="title" class="col-md-3">Task Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="title" name="title" value="{{$task->title}}"/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="description" class="col-md-3">Description</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" id="description" name="description" rows="3">{{$task->description}}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="priority" class="col-md-3">Priority</label>
                                    <div class="col-md-9">
                                        <div class="input-group mb-3">
                                            <select class="form-select" id="priority" name="priority">
                                                {{--                                                <option selected>Choose...</option>--}}
                                                <option {{ $task->priority == 'low' ? 'selected' : '' }} value="low"><span class="text-success">Low</span></option>
                                                <option {{ $task->priority == 'medium' ? 'selected' : '' }} value="medium"><span class="text-warning">Medium</span></option>
                                                <option {{ $task->priority == 'high' ? 'selected' : '' }} value="high"><span class="text-danger">High</span></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="date" class="col-md-3">Due Date</label>
                                    <div class="col-md-9">
                                        <div class="input-group date" id="datepicker">
                                            <input type="datetime-local" class="form-control" id="date" name="due_date" value="{{$task->due_date}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="status" class="col-md-3">Status</label>
                                    <div class="col-md-9">
                                        <div class="input-group mb-3">
                                            <select class="form-select" id="status" name="status">
                                                {{--                                                <option selected>Choose...</option>--}}
                                                <option {{ $task->status == 'toDo' ? 'selected' : '' }} value="toDo"><span class="text-success">To Do</span></option>
                                                <option {{ $task->status == 'inProgress' ? 'selected' : '' }} value="inProgress"><span class="text-warning">In-progress</span></option>
                                                <option {{ $task->status == 'completed' ? 'selected' : '' }} value="completed"><span class="text-danger">Completed</span></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <input type="submit" class="btn btn-success px-5" name="btn" value="Update Task"/>
                                        <a href="{{route('tasks.index')}}" class="btn btn-secondary px-5">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
