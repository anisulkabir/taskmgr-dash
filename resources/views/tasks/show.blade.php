@extends('layouts.app')

@section('helmet')
    All tasks
@endsection
{{--
<?php
echo '<pre>';
print_r($tasks);
echo '</pre>';
?>
--}}

@section('content')
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <p class="text-danger text-center my-1 fw-bold">{{Session::get('message')}} <span>&nbsp;</span></p>
                    <div class="card rounded-0">
                        <div class="card-header text-center fw-bolder h5">{{$task->title}} ({{$name}})</div>
                        <div class="card-body">
                            {{$task->description}}
                        </div>
                        <div>
                            <h5 class="card-subtitle">Associated persons;</h5>
                            <ul>
                                <li><img class="rounded-circle" src="{{$task->assignee->photo}}" alt="{{$task->assignee->name}}" title="{{$task->assignee->name}}" style="width: 32px;object-fit: cover"> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-5 text-center">
                <a href="{{route('tasks.index')}}" class="btn btn-secondary px-5">Close</a>
            </div>
        </div>
    </section>
@endsection
