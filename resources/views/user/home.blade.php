@extends('layouts.app')

@section('helmet')
    All tasks
@endsection
{{--
<?php
echo '<pre>';
print_r($tasks);
echo '</pre>';
?>
--}}
@section('content')
    <div class="container text-center">
        {{ __('You are logged in!') }}
    </div>
    <div class="container mt-5">
        <div class="card text-center">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a class="nav-link fw-bold {{$tab == 'active' ? 'active' : ''}}" aria-current="{{$tab == 'active' ? 'true' : ''}}
                        "href="{{route('dashboard')}}">Active Tasks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link fw-bold {{$tab == 'upcoming' ? 'active' : ''}}" aria-current="{{$tab == 'active' ? 'true' : ''}}
                        "href="{{route('upcoming')}}">Upcomimg Tasks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link fw-bold  {{$tab == 'complete' ? 'active' : ''}} aria-current="{{$tab == 'active' ? 'true' : ''}}
                        " href="{{route('complete')}}">Completed Tasks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link fw-bold {{$tab == 'backlog' ? 'active' : ''}} aria-current="{{$tab == 'active' ? 'true' : ''}}
                        " href="{{route('backlog')}}">Backlogs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link fw-bold  {{$tab == 'archived' ? 'active' : ''}} aria-current="{{$tab == 'active' ? 'true' : ''}}
                        " href="{{route('archived')}}">Archived tasks</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="container d-flex flex-wrap gap-2 pb-5">
                    @foreach($tasks as $task)
                        <div class="card
                            @switch($task->priority)
                            @case('low') {{__('bg-primary')}} @break
                            @case('medium') {{__('bg-success')}} @break
                            @case('high') {{__('bg-danger')}} @break
                            @default {{ __('') }} @endswitch
                        " style="width: 18rem;--bs-bg-opacity: .5;">
                        <div class="card-body"
                             style="height: 20rem;text-overflow: ellipsis; overflow: hidden; white-space: wrap">
                            <h5 class="card-title">{{$task->title}}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                            <p class="card-text">{{$task->description}}</p>
                        </div>
                        <div>

{{--                            <a href="#" class="card-link">Card link</a>--}}
{{--                            <a href="#" class="card-link">Another link</a>--}}
                        </div>
                </div>
                @endforeach

                {{--                    <div class="card" style="width: 18rem;">--}}
                {{--                        <div class="card-body">--}}
                {{--                            <h5 class="card-title">Task title</h5>--}}
                {{--                            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>--}}
                {{--                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
                {{--                            <a href="#" class="card-link">Card link</a>--}}
                {{--                            <a href="#" class="card-link">Another link</a>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}


            </div>

            <div class="d-flex justify-content-center">
                {!! $tasks->links() !!}
            </div>
            {{--                <nav aria-label="Page navigation example">--}}
            {{--                    <ul class="pagination justify-content-center">--}}
            {{--                        <li class="page-item disabled">--}}
            {{--                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="page-item"><a class="page-link" href="#">1</a></li>--}}
            {{--                        <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
            {{--                        <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
            {{--                        <li class="page-item">--}}
            {{--                            <a class="page-link" href="#">Next</a>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}

            {{--                </nav>--}}
            {{--                <h5 class="card-title">Special title treatment</h5>--}}
            {{--                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
            <a href="{{route('tasks.index')}}" class="btn btn-primary px-5">All tasks</a>
        </div>
    </div>
    </div>
@endsection
